/**
 * Created by Dhaval on 01/06/17.
 */
"use strict";

var 
User = module.parent.require("./user"),
Password = module.parent.require("./password"),
utils = module.parent.require("../public/src/utils"),
db = module.parent.require('./database'),
passport = module.parent.require("passport"),
passportLocal = module.parent.require('passport-local').Strategy,
request = module.parent.require("request"),
async = module.parent.require("async"),
meta = module.parent.require('./meta'),    

signinUrl = 'https://beta.igotthis.com/api/authenticateUser',
default_pwd = 'abc123';

(function(module) {
    
    var AuthOverrideLogin = {};
    AuthOverrideLogin.load = function(arg, next) {
        var app = arg.app;
        var router = arg.router;
        var middleware = arg.middleware;
        var controllers = arg.controllers;
        
        router.post("/user/login", controllers.authentication.login);
        router.get("/user/login", controllers.authentication.login);

        app.use(router);
    };

    AuthOverrideLogin.auth = function() {        
        passport.use(new passportLocal({
            passReqToCallback: true
        }, customSignin));
    };

    module.exports = AuthOverrideLogin;
})(module);

function signIn(body, callback) {
    request({
        method: 'POST',
        url: signinUrl,
        json: true,
        body: {name: body.userID, password: body.pw}
    }, function(err, msg, res) {        
        if (err) {
            callback(err);
            return;
        }        
        console.log("res:", res);
        if (msg.statusCode !== 200) {
            callback(msg.statusMessage);
            return;
        }
        
        // TODO: These data should come from IGT Auth service so that we can build the User Profile in NodeBB
        var user = {
            id: body.userID,
            pno: 11223344,
            userID: body.userID,
            email: body.userID
        };

        callback(null, user);
    });
}

function signinWithURL(req, username, password, next) {    

    password = unescape(password);
    password = password.replace(/\\{2}/, '\\');
    
    var uid, userData = {};

    async.waterfall([
        function(next) {            
            next(null, {
                userID: username,
                pw: password
            });
        },
        function(body, next) {
            
            if (body.pw) {
                body.pw = body.pw.replace(/\\\'/, "\'");
            }
            
            signIn(body, function(err, user) {
                if (err) {
                    console.log(err);
                    return next(new Error(err.text));
                }
                next(null, user);
            });

        },        
        function(userObj, next) {
            
            var uname;
            if (userObj && userObj.id) { 

                // TODO: Before Creating new User check if the User already exist
                // If exist, use that instead create New one

                var userId = '' + userObj.id;
                uname = Date.now() + '';                
                User.create({
                    username: uname,
                    password: default_pwd,
                    fullname: 'Test User',  
                    phone: userObj.pno
                }, function(err, _uid) {
                    if (err) {
                        console.log('error', err);
                        return next(new Error('[[error:no-user]]'));
                    }
                    uid = _uid;
                    User.auth.logAttempt(uid, req.ip, next);
                });
            } 

        },
        function(next) {
            
            async.parallel({
                userData: function(next) {
                    db.getObjectFields('user:' + uid, ['password', 'banned', 'passwordExpiry'], next);
                },
                isAdmin: function(next) {
                    User.isAdministrator(uid, next);
                }
            }, next);
        },
        function(result, next) {
            
            userData = result.userData;
            userData.uid = uid;

            // TODO: For NodeBB admin, we may need to add some fixed logins,
            // but that we can add on top so that for those users it would not go to IGT 
            // but instead check with NodeBB database only
            userData.isAdmin = result.isAdmin;

            if (!result.isAdmin && parseInt(meta.config.allowLocalLogin, 10) === 0) {
                return next(new Error('[[error:local-login-disabled]]'));
            }

            if (!userData || !userData.password) {
                return next(new Error('[[error:invalid-user-data]]'));
            }
            if (userData.banned && parseInt(userData.banned, 10) === 1) {
                return next(new Error('[[error:user-banned]]'));
            }
            
            Password.compare(default_pwd, userData.password, next);
        },

        function(passwordMatch, next) {
            if (!passwordMatch) {
                return next(new Error('[[error:invalid-password]]'));
            }
            User.auth.clearLoginAttempts(uid);
            next(null, userData, '[[success:authentication-successful]]');
        }
    ], next);
};

function customSignin(req, username, password, next) {
    console.log("username/password:", username, password);
    
    if (!username) {
        return next(new Error('[[error:invalid-username]]'));
    }

    return signinWithURL(req, username, password, next);    
}
