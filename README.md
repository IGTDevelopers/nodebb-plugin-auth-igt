# Install the plugin in NodeBB server

```
npm install <path>
```

# Activate Plugin

* Login to the NodeBB server with Admin Credentials, and goto Install Plugins
* Activate the nodebb-plugin-auth-igt plugin and restart NodeBB
* Current plugin is using the IGT Beta Site for Login but in future we can put a configuration file to get the target URL.

# Couple of Issues (not complex)

* IGT is not returning the Profile info, so need to send that, so that we can create User in NodeBB with proper inforamtion like Name
* Check the NodeBB user for the given Email first, so that we use existing user instead of creating new one
* Escape few users for Admin purpose, like our users for Admin Login to Manage NodeBB

# Compatibility

I have tested this plugin with latest version as well as 1.x so all good here

# Part of the OAuth implementation in IGT-Web is done based on this, need to test and validate

https://tech.zilverline.com/2017/03/17/nodejs-oauth2-provider